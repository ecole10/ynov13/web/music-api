# Contributing

## Branches

This project use [gitflow](https://danielkummer.github.io/git-flow-cheatsheet/index.html)

![gitflow](https://blog.engineering.publicissapient.fr/wp-content/uploads/2018/03/Image-768x537.png)

## Commits

This project use [semantics commit](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)
