package com.api.music.exception;

public class TokenExpiredException extends IllegalStateException {
    public TokenExpiredException() {
        super("Token expired, please check your email");
    }
}
