package com.api.music.exception;

import org.springframework.util.StringUtils;

public class UserAlreadyExistException extends IllegalStateException {
    public UserAlreadyExistException(String field, String value) {
        super(String.format("%s with value (%s) already exist", StringUtils.capitalize(field), value));
    }
}
