package com.api.music.exception;

public class AuthorizationExceptedException extends RuntimeException {
    public AuthorizationExceptedException() {
        super("Refresh token is missing");
    }
}
