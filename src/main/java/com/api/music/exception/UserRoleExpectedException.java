package com.api.music.exception;

public class UserRoleExpectedException extends RuntimeException {
    public UserRoleExpectedException() {
        super("User should have at least one role");
    }
}
