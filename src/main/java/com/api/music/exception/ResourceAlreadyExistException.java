package com.api.music.exception;

import org.springframework.util.StringUtils;

public class ResourceAlreadyExistException extends IllegalStateException {
    public ResourceAlreadyExistException(String existResource, String existResourceValue, String resource, String resourceValue) {
        super(String.format(
            "Resource (%s : %s) already exist in (%s : %s)",
            StringUtils.capitalize(existResource),
            existResourceValue,
            StringUtils.capitalize(resource),
            resourceValue
        ));
    }
}
