package com.api.music.exception;

import org.springframework.util.StringUtils;

public class ElementNotFoundInResourceException extends RuntimeException {
    public ElementNotFoundInResourceException(String notFoundResource, String notFoundResourceValue, String resource, String resourceValue) {
        super(String.format(
                "Resource (%s : %s) not found in (%s : %s)",
                StringUtils.capitalize(notFoundResource),
                notFoundResourceValue,
                StringUtils.capitalize(resource),
                resourceValue
        ));
    }
}
