package com.api.music.exception;

public class EmailNotSendException extends RuntimeException {
    public EmailNotSendException() {
        super("Failed to send email");
    }
}
