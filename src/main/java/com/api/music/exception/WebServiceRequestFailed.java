package com.api.music.exception;

import com.api.music.enums.Platform;

public class WebServiceRequestFailed extends RuntimeException {
    public WebServiceRequestFailed(Platform platform, Integer responseStatus, String path) {
        super(String.format("Webservice (%s) return (%s) for request (%s)", platform.toString(), responseStatus.toString(), path));
    }
}
