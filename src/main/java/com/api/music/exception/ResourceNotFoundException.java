package com.api.music.exception;

import org.springframework.util.StringUtils;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String type, Object id) {
        super(String.format("Resource (%s) with identifier (%s) not found", StringUtils.capitalize(type), id));
    }
}
