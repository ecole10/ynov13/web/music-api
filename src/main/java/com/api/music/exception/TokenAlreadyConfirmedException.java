package com.api.music.exception;

public class TokenAlreadyConfirmedException extends IllegalStateException {
    public TokenAlreadyConfirmedException() {
        super("Token already confirmed");
    }
}
