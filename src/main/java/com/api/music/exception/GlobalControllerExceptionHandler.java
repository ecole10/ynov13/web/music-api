package com.api.music.exception;

import com.api.music.utils.JsonResponse;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {
    @ExceptionHandler(ConversionFailedException.class)
    @ResponseStatus(BAD_REQUEST)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleConflict(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), BAD_REQUEST);
    }

    @ExceptionHandler(AuthorizationExceptedException.class)
    @ResponseStatus(UNAUTHORIZED)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleAuthorization(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), UNAUTHORIZED);
    }

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(FORBIDDEN)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleForbidden(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), FORBIDDEN);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleResourceNotFound(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), NOT_FOUND);
    }

    @ExceptionHandler(ElementNotFoundInResourceException.class)
    @ResponseStatus(NOT_FOUND)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleElementNotFoundInResource(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleViolationsException(BindException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(ResourceAlreadyExistException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleResourceAlreadyExist(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleUserAlreadyExist(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(UserRoleExpectedException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleUserRoleExpected(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(TokenAlreadyConfirmedException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleTokenAlreadyConfirmed(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(TokenExpiredException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleTokenExpired(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(WebServiceRequestFailed.class)
    @ResponseStatus(SERVICE_UNAVAILABLE)
    @Operation(hidden = true)
    public ResponseEntity<JsonResponse> handleWebServiceRequestFailed(RuntimeException ex) {
        return new ResponseEntity<>(new JsonResponse(ex.getMessage()), SERVICE_UNAVAILABLE);
    }
}
