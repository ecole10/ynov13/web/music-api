package com.api.music.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "track", schema = "public")
public class Track {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    private String platformId;

    private String platform;

    private String title;

    private String link;

    private String duration;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "playlist_track",
            joinColumns = @JoinColumn(name = "track_id"),
            inverseJoinColumns = @JoinColumn(name = "playlist_id")
    )
    private Collection<Playlist> playlists = new ArrayList<>();

    public Boolean isContainsPlaylist(Playlist playlist) {
        return playlists.contains(playlist);
    }
}
