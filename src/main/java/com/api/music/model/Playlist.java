package com.api.music.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import static com.api.music.utils.DateUtils.secondsToMinutes;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "playlist", schema = "public")
public class Playlist {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "playlist_track",
            joinColumns = @JoinColumn(name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "track_id")
    )
    private Collection<Track> tracks = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Boolean isContainsTrack(Track track) {
        return tracks.contains(track);
    }

    public String duration() {
        long totalDuration = 0L;

        for (Track track : tracks) {
            String trackDuration = track.getDuration();
            long minutes = Long.parseLong(trackDuration.substring(0, 2));
            long seconds = Long.parseLong(trackDuration.substring(3));
            totalDuration += (minutes * 60) + seconds;
        }

        return secondsToMinutes(totalDuration);
    }
}
