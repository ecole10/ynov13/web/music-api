package com.api.music.filter;

import com.api.music.exception.ForbiddenException;
import com.api.music.service.JWTService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.api.music.enums.Security.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@Slf4j
public class JWTAuthorizationFilter extends OncePerRequestFilter {
    private final JWTService jwtService;

    public JWTAuthorizationFilter(JWTService jwtService) {
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException, ForbiddenException {
        if (!request.getServletPath().equals(LOGIN_URL.value()) &&
            !request.getServletPath().equals(REFRESH_TOKEN_URL.value())
        ) {
            String authorizationHeader = request.getHeader(AUTHORIZATION);

            if (authorizationHeader != null && authorizationHeader.startsWith(TOKEN_PREFIX.value())) {
                try {
                    SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                        jwtService.getUsername(authorizationHeader),
                        null,
                        jwtService.getAuthorities(authorizationHeader)
                    ));
                } catch (Exception e) {
                    log.error("Error logging in : {}", e.getMessage());
                    response.setStatus(FORBIDDEN.value());
                }
            }
        }

        filterChain.doFilter(request, response);
    }
}
