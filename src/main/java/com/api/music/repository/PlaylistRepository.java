package com.api.music.repository;

import com.api.music.model.Playlist;
import com.api.music.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, UUID> {
    List<Playlist> findAllByUserId(UUID userId);
    Optional<Playlist> findByIdAndUser(UUID playlistId, User user);
}
