package com.api.music.repository;

import com.api.music.model.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TrackRepository extends JpaRepository<Track, UUID> {
    Track findByPlatformAndPlatformId(String platform, String platformId);
    Boolean existsByPlatformAndPlatformId(String platform, String platformId);
}
