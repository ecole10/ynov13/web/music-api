package com.api.music.webservice.deezer.api.chart;

import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.deezer.DeezerClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.api.music.enums.Platform.DEEZER;

@Service
@RequiredArgsConstructor
public class DeezerGetChart {
    private final DeezerClient deezerClient;

    public JsonNode send(String type) {
        JsonNode response = deezerClient.get("/chart/0/" + type);

        if (response.has("error")) {
            throw new WebServiceRequestFailed(DEEZER, response.get("error").get("code").intValue(), "/chart/0/" + type);
        }

        return response;
    }
}
