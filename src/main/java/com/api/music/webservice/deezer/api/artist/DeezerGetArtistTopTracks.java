package com.api.music.webservice.deezer.api.artist;

import com.api.music.exception.ResourceNotFoundException;
import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.deezer.DeezerClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.api.music.enums.DeezerCodeError.DATA_NOT_FOUND;
import static com.api.music.enums.Platform.DEEZER;
import static com.api.music.enums.SearchType.ARTIST;

@Service
@RequiredArgsConstructor
public class DeezerGetArtistTopTracks {
    private final DeezerClient deezerClient;

    public JsonNode send(String id) {
        JsonNode response = deezerClient.get("/artist/" + id + "/top");

        if (response.has("error")) {
            if (response.get("error").get("code").intValue() == DATA_NOT_FOUND.value()) {
                throw new ResourceNotFoundException(ARTIST.value(), id);
            } else {
                throw new WebServiceRequestFailed(DEEZER, response.get("error").get("code").intValue(), "/artist/" + id + "/top");
            }
        }

        return response;
    }
}
