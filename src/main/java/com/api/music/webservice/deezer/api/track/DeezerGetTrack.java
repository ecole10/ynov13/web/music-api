package com.api.music.webservice.deezer.api.track;

import com.api.music.exception.ResourceNotFoundException;
import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.deezer.DeezerClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.api.music.enums.DeezerCodeError.QUERY_INVALID;
import static com.api.music.enums.Platform.DEEZER;
import static com.api.music.enums.SearchType.TRACK;

@Service
@RequiredArgsConstructor
public class DeezerGetTrack {
    private final DeezerClient deezerClient;

    public JsonNode send(String id) {
        JsonNode response = deezerClient.get("/track/" + id);

        if (response.has("error")) {
            if (response.get("error").get("code").intValue() == QUERY_INVALID.value()) {
                throw new ResourceNotFoundException(TRACK.value(), id);
            } else {
                throw new WebServiceRequestFailed(DEEZER, response.get("error").get("code").intValue(), "/track/" + id);
            }
        }

        return response;
    }
}
