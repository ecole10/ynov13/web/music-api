package com.api.music.webservice.deezer.api.search;

import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.deezer.DeezerClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static com.api.music.enums.Platform.DEEZER;

@Service
@RequiredArgsConstructor
public class DeezerGetSearch {
    private final DeezerClient deezerClient;

    public JsonNode send(String search, String type) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q", search);
        JsonNode response = deezerClient.get("/search/" + type, params);

        if (response.has("error")) {
            throw new WebServiceRequestFailed(DEEZER, response.get("error").get("code").intValue(), "/search/" + type);
        }

        return response;
    }
}
