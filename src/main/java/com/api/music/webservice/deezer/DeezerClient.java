package com.api.music.webservice.deezer;

import com.api.music.webservice.AbstractClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import static io.netty.handler.codec.http.HttpScheme.HTTPS;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @see <a href="https://developers.deezer.com/api/explorer">Deezer API docs</a>
 */
@Service
@RequiredArgsConstructor
public class DeezerClient extends AbstractClient {
    private final static String BASE_URL = "api.deezer.com";

    @Override
    public JsonNode get(String uri) {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
                .scheme(HTTPS.name().toString())
                .host(BASE_URL)
                .path(uri)
                .build())
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .header(ACCEPT, APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(JsonNode.class)
            .block();
    }

    @Override
    public JsonNode get(String uri, MultiValueMap<String, String> params) {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
                .scheme(HTTPS.name().toString())
                .host(BASE_URL)
                .path(uri)
                .queryParams(params)
                .build())
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .header(ACCEPT, APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(JsonNode.class)
            .block();
    }

    @Override
    protected String getAccessToken(String clientId, String clientSecret, String tokenUrl, String grantType) {
        return null;
    }
}
