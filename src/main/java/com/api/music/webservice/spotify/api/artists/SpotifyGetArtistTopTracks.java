package com.api.music.webservice.spotify.api.artists;

import com.api.music.exception.ResourceNotFoundException;
import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.spotify.SpotifyClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import static com.api.music.enums.Platform.SPOTIFY;
import static com.api.music.enums.SearchType.ARTIST;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
@RequiredArgsConstructor
public class SpotifyGetArtistTopTracks {
    private final SpotifyClient spotifyClient;

    public JsonNode send(String id) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("market", "FR");

        try {
            return spotifyClient.get("/v1/artists/" + id + "/top-tracks", params);
        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == BAD_REQUEST) {
                throw new ResourceNotFoundException(ARTIST.value(), id);
            } else {
                throw new WebServiceRequestFailed(SPOTIFY, e.getStatusCode().value(), "/v1/artists/" + id + "/top-tracks");
            }
        }
    }
}
