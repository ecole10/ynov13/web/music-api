package com.api.music.webservice.spotify.api.artists;

import com.api.music.exception.ResourceNotFoundException;
import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.spotify.SpotifyClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import static com.api.music.enums.Platform.SPOTIFY;
import static com.api.music.enums.SearchType.ARTIST;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
@RequiredArgsConstructor
public class SpotifyGetArtist {
    private final SpotifyClient spotifyClient;

    public JsonNode send(String id) {
        try {
            return spotifyClient.get("/v1/artists/" + id);
        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == BAD_REQUEST) {
                throw new ResourceNotFoundException(ARTIST.value(), id);
            } else {
                throw new WebServiceRequestFailed(SPOTIFY, e.getStatusCode().value(), "/v1/artists/" + id);
            }
        }
    }
}
