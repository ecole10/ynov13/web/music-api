package com.api.music.webservice.spotify.api.playlist;

import com.api.music.exception.ResourceNotFoundException;
import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.spotify.SpotifyClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import static com.api.music.enums.Platform.SPOTIFY;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
@RequiredArgsConstructor
public class SpotifyGetPlaylistTracks {
    private final SpotifyClient spotifyClient;

    public JsonNode send(String id) {
        try {
            return spotifyClient.get("/v1/playlists/" + id + "/tracks");
        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == BAD_REQUEST) {
                throw new ResourceNotFoundException("Playlist", id);
            } else {
                throw new WebServiceRequestFailed(SPOTIFY, e.getStatusCode().value(), "/v1/playlists/" + id + "/tracks");
            }
        }
    }

    public JsonNode send(String id, String limit) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("limit", limit);

        try {
            return spotifyClient.get("/v1/playlists/" + id + "/tracks", params);
        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == BAD_REQUEST) {
                throw new ResourceNotFoundException("Playlist", id);
            } else {
                throw new WebServiceRequestFailed(SPOTIFY, e.getStatusCode().value(), "/v1/playlists/" + id + "/tracks");
            }
        }
    }
}
