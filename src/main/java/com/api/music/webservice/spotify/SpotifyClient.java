package com.api.music.webservice.spotify;

import com.api.music.webservice.AbstractClient;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;

import static com.api.music.enums.Security.TOKEN_PREFIX;
import static io.netty.handler.codec.http.HttpScheme.HTTPS;
import static org.springframework.http.HttpHeaders.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @see <a href="https://developer.spotify.com/console">Spotify API docs</a>
 */
@Service
public class SpotifyClient extends AbstractClient {
    private final static String TOKEN_URL = "https://accounts.spotify.com/api/token";
    private final static String BASE_URL = "api.spotify.com";
    private final String accessToken;

    public SpotifyClient(
        @Value("${spring.security.oauth2.client.registration.spotify.client-id}") String clientId,
        @Value("${spring.security.oauth2.client.registration.spotify.client-secret}") String clientSecret,
        @Value("${spring.security.oauth2.client.registration.spotify.authorization-grant-type}") String grantType
    ) {
        this.accessToken = getAccessToken(clientId, clientSecret, TOKEN_URL, grantType);
    }

    @Override
    public JsonNode get(String uri) {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
                .scheme(HTTPS.name().toString())
                .host(BASE_URL)
                .path(uri)
                .build())
            .header(AUTHORIZATION, TOKEN_PREFIX.value() + accessToken)
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .header(ACCEPT, APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(JsonNode.class)
            .block();
    }

    @Override
    public JsonNode get(String uri, MultiValueMap<String, String> params) {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
                .scheme(HTTPS.name().toString())
                .host(BASE_URL)
                .path(uri)
                .queryParams(params)
                .build())
            .header(AUTHORIZATION, TOKEN_PREFIX.value() + accessToken)
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .header(ACCEPT, APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(JsonNode.class)
            .block();
    }

    @Override
    protected String getAccessToken(String clientId, String clientSecret, String tokenUrl, String grantType) {
        return webClient.post()
            .uri(tokenUrl)
            .header(AUTHORIZATION, "Basic " + Base64Utils.encodeToString((clientId + ':' + clientSecret).getBytes()))
            .body(BodyInserters.fromFormData("grant_type", grantType))
            .retrieve()
            .bodyToMono(JsonNode.class)
            .map(tokenResponse -> tokenResponse.get("access_token").textValue())
            .block();
    }
}
