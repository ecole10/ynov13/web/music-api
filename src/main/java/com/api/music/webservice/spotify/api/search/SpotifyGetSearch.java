package com.api.music.webservice.spotify.api.search;

import com.api.music.exception.WebServiceRequestFailed;
import com.api.music.webservice.spotify.SpotifyClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import static com.api.music.enums.Platform.SPOTIFY;

@Service
@RequiredArgsConstructor
public class SpotifyGetSearch {
    private final SpotifyClient spotifyClient;

    public JsonNode send(String search, String type) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q", search);
        params.add("type", type);
        params.add("market", "FR");

        try {
            return spotifyClient.get("/v1/search", params);
        } catch (WebClientResponseException e) {
            throw new WebServiceRequestFailed(SPOTIFY, e.getStatusCode().value(), "/v1/search");
        }
    }
}
