package com.api.music.webservice;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

public abstract class AbstractClient {
    private final Integer MAX_MEMORY_SIZE_IN_KB = 64;
    protected final WebClient webClient;

    protected AbstractClient() {
        this.webClient = WebClient.builder()
            .exchangeStrategies(ExchangeStrategies.builder()
                .codecs(configurer -> configurer
                    .defaultCodecs()
                    .maxInMemorySize(MAX_MEMORY_SIZE_IN_KB * 1000)
                ).build()
            ).build();
    }

    public abstract JsonNode get(String uri);
    public abstract JsonNode get(String uri, MultiValueMap<String, String> params);

    protected abstract String getAccessToken(String clientId, String clientSecret, String tokenUrl, String grantType);
}
