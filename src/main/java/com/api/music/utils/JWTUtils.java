package com.api.music.utils;

import com.api.music.dto.output.auth.JWTOutputDto;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.stream.Collectors;

import static com.api.music.enums.Security.TOKEN_PREFIX;
import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

@Service
@RequiredArgsConstructor
public class JWTUtils {
    @Value("${jwt.secret}")
    private String jwtTokenSecret;
    @Value("${jwt.access-token.expire}")
    private Integer accessTokenLifetime;
    @Value("${jwt.refresh-token.expire}")
    private Integer refreshTokenLifetime;

    public String createAccessToken(UserDetails user, String requestUrl) {
        return JWT.create()
            .withSubject(user.getUsername())
            .withNotBefore(new Date())
            .withExpiresAt(getTokenLifetime(accessTokenLifetime))
            .withIssuer(requestUrl)
            .withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
            .sign(getAlgorithm());
    }

    public String createRefreshToken(String username, String requestUrl) {
        return JWT.create()
            .withSubject(username)
            .withNotBefore(new Date())
            .withExpiresAt(getTokenLifetime(refreshTokenLifetime))
            .withIssuer(requestUrl)
            .sign(getAlgorithm());
    }

    public DecodedJWT verifyToken(String token) {
        return JWT.require(getAlgorithm()).build().verify(token);
    }

    public String extractUsernameFromToken(String token) {
        return verifyToken(token).getSubject();
    }

    public JWTOutputDto formatResponseTokens(String accessToken, String refreshToken) {
        return new JWTOutputDto()
            .setAccessToken(TOKEN_PREFIX.value() + accessToken)
            .setRefreshToken(TOKEN_PREFIX.value() + refreshToken);
    }

    private Date getTokenLifetime(int tokenLifetime) {
        return new Date(System.currentTimeMillis() + (long) tokenLifetime * 60 * 1000);
    }

    private Algorithm getAlgorithm() {
        return HMAC512(jwtTokenSecret.getBytes());
    }
}
