package com.api.music.utils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {
    public static String secondsToMinutes(Long seconds) {
        return LocalTime.MIN.plusSeconds(seconds).format(DateTimeFormatter.ofPattern("mm:ss"));
    }
}
