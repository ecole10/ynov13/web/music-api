package com.api.music.mapper;

import com.api.music.dto.output.user.playlist.UserPlaylistTrackOutputDto;
import com.api.music.model.Track;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserTrackMapper {
    UserPlaylistTrackOutputDto trackToUserPlaylistTrackOutputDto(Track track);
    List<UserPlaylistTrackOutputDto> tracksToUserPlaylistTrackOutputDtos(List<Track> tracks);
}
