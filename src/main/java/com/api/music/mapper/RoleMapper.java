package com.api.music.mapper;

import com.api.music.dto.output.role.RoleOutputDto;
import com.api.music.model.Role;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    RoleOutputDto roleToRoleOutputDto(Role role);
    List<RoleOutputDto> rolesToRoleOutputDtos(List<Role> roles);
}
