package com.api.music.mapper;

import com.api.music.dto.input.user.UserInputDto;
import com.api.music.dto.output.user.UserOutputDto;
import com.api.music.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = RoleMapper.class)
public interface UserMapper {
    User userInputDtoToUser(UserInputDto userInputDto);
    UserOutputDto userToUserOutputDto(User user);
    List<UserOutputDto> usersToUserOutputDtos(List<User> users);
}
