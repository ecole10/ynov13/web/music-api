package com.api.music.mapper.spotify;

import com.api.music.dto.output.artist.ArtistDetailsOutputDto;
import com.api.music.dto.output.artist.ArtistOutputDto;
import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

import static com.api.music.enums.Platform.SPOTIFY;

public class SpotifyArtistMapper {
    private final SpotifyTrackMapper spotifyTrackMapper = new SpotifyTrackMapper();

    public List<ArtistOutputDto> artistsToArtistOutputDtos(JsonNode artists) {
        List<ArtistOutputDto> artistOutputDtos = new ArrayList<>();

        for (JsonNode artist : artists.get("artists").get("items")) {
            artistOutputDtos.add(artistToArtistOutputDto(artist));
        }

        return artistOutputDtos;
    }

    public List<TrackDetailsOutputDto> artistTopTracksToTrackDetailsOutputDtos(JsonNode tracks) {
        List<TrackDetailsOutputDto> trackDetailsOutputDtos = new ArrayList<>();

        for (JsonNode track : tracks.get("tracks")) {
            trackDetailsOutputDtos.add(spotifyTrackMapper.trackDetailsToTrackDetailsOutputDto(track));
        }

        return trackDetailsOutputDtos;
    }

    public ArtistDetailsOutputDto artistDetailsToArtistDetailsOutputDto(JsonNode artist) {
        return new ArtistDetailsOutputDto()
            .setId(artist.get("id").textValue())
            .setName(artist.get("name").textValue())
            .setFollowers(artist.get("followers").get("total").intValue())
            .setLink(artist.get("external_urls").get("spotify").textValue())
            .setPlatform(SPOTIFY.toString());
    }

    private ArtistOutputDto artistToArtistOutputDto(JsonNode artist) {
        return new ArtistOutputDto()
            .setId(artist.get("id").textValue())
            .setName(artist.get("name").textValue())
            .setPlatform(SPOTIFY.toString());
    }
}
