package com.api.music.mapper.spotify;

import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.dto.output.track.TrackOutputDto;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

import static com.api.music.enums.Platform.SPOTIFY;
import static com.api.music.utils.DateUtils.secondsToMinutes;

public class SpotifyTrackMapper {
    public List<TrackOutputDto> tracksToTrackOutputDtos(JsonNode tracks) {
        List<TrackOutputDto> trackOutputDtos = new ArrayList<>();

        for (JsonNode track : tracks.get("tracks").get("items")) {
            trackOutputDtos.add(trackToTrackOutputDto(track));
        }

        return trackOutputDtos;
    }

    public List<TrackOutputDto> playlistTracksToTrackOutputDtos(JsonNode tracks) {
        List<TrackOutputDto> trackOutputDtos = new ArrayList<>();

        for (JsonNode track : tracks.get("items")) {
            trackOutputDtos.add(trackToTrackOutputDto(track.get("track")));
        }

        return trackOutputDtos;
    }

    public TrackDetailsOutputDto trackDetailsToTrackDetailsOutputDto(JsonNode track) {
        return new TrackDetailsOutputDto()
            .setId(track.get("id").textValue())
            .setTitle(track.get("name").textValue())
            .setDuration(secondsToMinutes(track.get("duration_ms").longValue() / 1000))
            .setLink(track.get("external_urls").get("spotify").textValue())
            .setArtists(getArtists(track.get("artists")).toArray(new String[0]))
            .setAlbum(track.get("album").get("name").textValue())
            .setPlatform(SPOTIFY.toString());
    }

    private TrackOutputDto trackToTrackOutputDto(JsonNode track) {
        return new TrackOutputDto()
            .setId(track.get("id").textValue())
            .setTitle(track.get("name").textValue())
            .setArtists(getArtists(track.get("artists")).toArray(new String[0]))
            .setAlbum(track.get("album").get("name").textValue())
            .setAlbumCover(track.get("album").get("images").get(0).get("url").textValue())
            .setLink(track.get("external_urls").get("spotify").textValue())
            .setPreview(track.get("preview_url").textValue())
            .setPlatform(SPOTIFY.toString());
    }

    private List<String> getArtists(JsonNode artists) {
        List<String> artistsName = new ArrayList<>();

        for (JsonNode artist : artists) {
            artistsName.add(artist.get("name").textValue());
        }

        return artistsName;
    }
}
