package com.api.music.mapper;

import com.api.music.dto.input.user.playlist.UserPlaylistInputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistOutputDto;
import com.api.music.model.Playlist;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserPlaylistMapper {
    Playlist userPlaylistInputDtoToPlaylist(UserPlaylistInputDto userPlaylistInputDto);
    UserPlaylistOutputDto playlistToUserPlaylistOutputDto(Playlist playlist);
    List<UserPlaylistOutputDto> playlistsToUserPlaylistOutputDtos(List<Playlist> playlists);
}
