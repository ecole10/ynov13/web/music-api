package com.api.music.mapper.deezer;

import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.dto.output.track.TrackOutputDto;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

import static com.api.music.enums.Platform.DEEZER;
import static com.api.music.utils.DateUtils.secondsToMinutes;

public class DeezerTrackMapper {
    public List<TrackOutputDto> tracksToTrackOutputDtos(JsonNode tracks) {
        List<TrackOutputDto> trackOutputDtos = new ArrayList<>();

        for (JsonNode item : tracks.get("data")) {
            trackOutputDtos.add(trackToTrackOutputDto(item));
        }

        return trackOutputDtos;
    }

    public TrackDetailsOutputDto trackDetailsToTrackDetailsOutputDto(JsonNode track) {
        return new TrackDetailsOutputDto()
            .setId(track.get("id").toString())
            .setTitle(track.get("title").textValue())
            .setDuration(secondsToMinutes(track.get("duration").longValue()))
            .setLink(track.get("link").textValue())
            .setArtists(new String[]{track.get("artist").get("name").textValue()})
            .setAlbum(track.get("album").get("title").textValue())
            .setPlatform(DEEZER.toString());
    }

    private TrackOutputDto trackToTrackOutputDto(JsonNode track) {
        return new TrackOutputDto()
            .setId(track.get("id").toString())
            .setTitle(track.get("title").textValue())
            .setArtists(new String[]{track.get("artist").get("name").textValue()})
            .setAlbum(track.get("album").get("title").textValue())
            .setAlbumCover(track.get("album").get("cover_medium").textValue())
            .setLink(track.get("link").textValue())
            .setPreview(track.get("preview").textValue())
            .setPlatform(DEEZER.toString());
    }
}
