package com.api.music.mapper.deezer;

import com.api.music.dto.output.artist.ArtistDetailsOutputDto;
import com.api.music.dto.output.artist.ArtistOutputDto;
import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

import static com.api.music.enums.Platform.DEEZER;

public class DeezerArtistMapper {
    private final DeezerTrackMapper deezerTrackMapper = new DeezerTrackMapper();

    public List<ArtistOutputDto> artistsToArtistOutputDtos(JsonNode artists) {
        List<ArtistOutputDto> artistOutputDtos = new ArrayList<>();

        for (JsonNode artist : artists.get("data")) {
            artistOutputDtos.add(artistToArtistOutputDto(artist));
        }

        return artistOutputDtos;
    }

    public List<TrackDetailsOutputDto> artistTopTracksToTrackDetailsOutputDtos(JsonNode tracks) {
        List<TrackDetailsOutputDto> trackDetailsOutputDtos = new ArrayList<>();

        for (JsonNode track : tracks.get("data")) {
            trackDetailsOutputDtos.add(deezerTrackMapper.trackDetailsToTrackDetailsOutputDto(track));
        }

        return trackDetailsOutputDtos;
    }

    public ArtistDetailsOutputDto artistDetailsToArtistDetailsOutputDto(JsonNode artist) {
        return new ArtistDetailsOutputDto()
                .setId(artist.get("id").toString())
                .setName(artist.get("name").textValue())
                .setFollowers(artist.get("nb_fan").intValue())
                .setLink(artist.get("link").textValue())
                .setPlatform(DEEZER.toString());
    }

    private ArtistOutputDto artistToArtistOutputDto(JsonNode artist) {
        return new ArtistOutputDto()
                .setId(artist.get("id").toString())
                .setName(artist.get("name").textValue())
                .setPlatform(DEEZER.toString());
    }
}
