package com.api.music.controller;

import com.api.music.dto.input.user.UserRoleInputDto;
import com.api.music.dto.output.user.UserOutputDto;
import com.api.music.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "User")
@SecurityRequirement(name = "bearer-token")
@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    @Operation(summary = "Get users", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = UserOutputDto.class)))),
        @ApiResponse(responseCode = "403", content = @Content)
    })
    public ResponseEntity<List<UserOutputDto>> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), OK);
    }

    @GetMapping("/me")
    @Operation(summary = "Get user's information", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserOutputDto.class))),
        @ApiResponse(responseCode = "403", content = @Content)
    })
    public ResponseEntity<UserOutputDto> getCurrentUser(Principal principal) {
        return new ResponseEntity<>(userService.getCurrentUser(principal.getName()), OK);
    }

    @PatchMapping("/{user-id}")
    @Operation(summary = "Update user's role", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserOutputDto.class))),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "403", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content)
    })
    public ResponseEntity<UserOutputDto> updateUserRole(
        @PathVariable(name = "user-id") UUID userId,
        @Valid @RequestBody UserRoleInputDto userRoleInputDto
    ) {
        return new ResponseEntity<>(userService.updateUserRole(userId, userRoleInputDto.getRole().toString()), OK);
    }

    @DeleteMapping("/{user-id}")
    @Operation(summary = "Delete user", responses = {
        @ApiResponse(responseCode = "204", content = @Content),
        @ApiResponse(responseCode = "403", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content)
    })
    public ResponseEntity<Void> deleteUser(@PathVariable(name = "user-id") UUID userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(NO_CONTENT);
    }
}
