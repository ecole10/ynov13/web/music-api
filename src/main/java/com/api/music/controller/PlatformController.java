package com.api.music.controller;

import com.api.music.dto.output.track.TrackOutputDto;
import com.api.music.enums.Platform;
import com.api.music.service.TrackService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Platform")
@RestController
@RequestMapping("/api/platforms")
@RequiredArgsConstructor
public class PlatformController {
    private final TrackService trackService;

    @GetMapping
    @Operation(summary = "List of platforms available", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Platform.class))))
    })
    public ResponseEntity<Platform[]> getPlatforms() {
        return new ResponseEntity<>(Platform.values(), OK);
    }

    @GetMapping("/trends/tracks")
    @Operation(summary = "Get top 10 tracks", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = TrackOutputDto.class)))),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "503", content = @Content)
    })
    public ResponseEntity<List<TrackOutputDto>> getTrendsTracks(@RequestParam(required = false) Platform platform) {
        return new ResponseEntity<>(trackService.getTrendsTracks(platform), OK);
    }
}
