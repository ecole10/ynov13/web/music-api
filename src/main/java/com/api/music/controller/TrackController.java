package com.api.music.controller;

import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.enums.Platform;
import com.api.music.service.TrackService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Track")
@RestController
@RequestMapping("/api/tracks")
@RequiredArgsConstructor
public class TrackController {
    private final TrackService trackService;

    @GetMapping("/{track-id}")
    @Operation(summary = "Get track by id on specific platform", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = TrackDetailsOutputDto.class))),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content),
        @ApiResponse(responseCode = "503", content = @Content)
    })
    public ResponseEntity<TrackDetailsOutputDto> getTrack(
        @PathVariable(name = "track-id") String trackId,
        @RequestParam Platform platform
    ) {
        return new ResponseEntity<>(trackService.getTrack(trackId, platform), OK);
    }
}
