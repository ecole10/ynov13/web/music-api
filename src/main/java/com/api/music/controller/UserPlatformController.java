package com.api.music.controller;

import com.api.music.dto.output.user.platform.UserPlatformTracksOutputDto;
import com.api.music.service.UserPlaylistService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "User")
@SecurityRequirement(name = "bearer-token")
@RestController
@RequestMapping("/api/users/me/platforms")
@RequiredArgsConstructor
public class UserPlatformController {
    private final UserPlaylistService playlistService;

    @GetMapping("/tracks")
    @Operation(summary = "Get user's number of tracks by platform", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = UserPlatformTracksOutputDto.class)))),
        @ApiResponse(responseCode = "403", content = @Content)
    })
    public ResponseEntity<List<UserPlatformTracksOutputDto>> getNumberOfTracksByPlatform(Principal principal) {
        return new ResponseEntity<>(playlistService.getNumberOfTracksByPlatform(principal.getName()), OK);
    }
}
