package com.api.music.controller;

import com.api.music.dto.output.artist.ArtistDetailsOutputDto;
import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.enums.Platform;
import com.api.music.service.ArtistService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Artist")
@RestController
@RequestMapping("/api/artists")
@RequiredArgsConstructor
public class ArtistController {
    private final ArtistService artistService;

    @GetMapping("/{artist-id}")
    @Operation(summary = "Get artist by id on specific platform", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ArtistDetailsOutputDto.class))),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content),
        @ApiResponse(responseCode = "503", content = @Content)
    })
    public ResponseEntity<ArtistDetailsOutputDto> getArtist(
        @PathVariable(name = "artist-id") String artistId,
        @RequestParam Platform platform
    ) {
        return new ResponseEntity<>(artistService.getArtist(artistId, platform), OK);
    }

    @GetMapping("/{artist-id}/tracks")
    @Operation(summary = "Get artist's top tracks on specific platform", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = TrackDetailsOutputDto.class)))),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content),
        @ApiResponse(responseCode = "503", content = @Content)
    })
    public ResponseEntity<List<TrackDetailsOutputDto>> getArtistTopTracks(
        @PathVariable(name = "artist-id") String artistId,
        @RequestParam Platform platform
    ) {
        return new ResponseEntity<>(artistService.getArtistTopTracks(artistId, platform), OK);
    }
}
