package com.api.music.controller;

import com.api.music.dto.output.artist.ArtistOutputDto;
import com.api.music.dto.output.track.TrackOutputDto;
import com.api.music.enums.Platform;
import com.api.music.service.ArtistService;
import com.api.music.service.TrackService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Search")
@RestController
@RequestMapping("/api/search")
@RequiredArgsConstructor
public class SearchController {
    private final TrackService trackService;
    private final ArtistService artistService;

    @GetMapping("/tracks")
    @Operation(summary = "Search tracks", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = TrackOutputDto.class)))),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "503", content = @Content)
    })
    public ResponseEntity<List<TrackOutputDto>> searchTracks(
        @RequestParam String search,
        @RequestParam(required = false) Platform platform
    ) {
        return new ResponseEntity<>(trackService.searchTracks(search, platform), OK);
    }

    @GetMapping("/artists")
    @Operation(summary = "Search artists", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = ArtistOutputDto.class)))),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "503", content = @Content)
    })
    public ResponseEntity<List<ArtistOutputDto>> searchArtists(
        @RequestParam String search,
        @RequestParam(required = false) Platform platform
    ) {
        return new ResponseEntity<>(artistService.searchArtists(search, platform), OK);
    }
}
