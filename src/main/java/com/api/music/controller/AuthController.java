package com.api.music.controller;

import com.api.music.dto.input.user.UserInputDto;
import com.api.music.dto.output.auth.JWTOutputDto;
import com.api.music.dto.output.user.UserOutputDto;
import com.api.music.exception.ForbiddenException;
import com.api.music.service.ConfirmationTokenService;
import com.api.music.service.JWTService;
import com.api.music.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Auth")
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    @Value("${front.application.url}")
    private String frontApplicationBaseUrl;
    private final JWTService jwtService;
    private final UserService userService;
    private final ConfirmationTokenService confirmationTokenService;

    @PostMapping("/register")
    @Operation(summary = "Register", responses = {
        @ApiResponse(responseCode = "201", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserOutputDto.class))),
        @ApiResponse(responseCode = "422", content = @Content)
    })
    public ResponseEntity<UserOutputDto> register(@Valid @RequestBody UserInputDto userInputDto) {
        return new ResponseEntity<>(userService.createUser(userInputDto), CREATED);
    }

    @GetMapping("/register/confirm")
    @Operation(summary = "Confirm the register token", responses = {
        @ApiResponse(responseCode = "204", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content),
        @ApiResponse(responseCode = "422", content = @Content)
    })
    public ResponseEntity<Void> confirm(@RequestParam("token") String token, HttpServletResponse response) throws IOException {
        confirmationTokenService.confirmToken(token);
        response.sendRedirect(frontApplicationBaseUrl + "/login");
        return new ResponseEntity<>(NO_CONTENT);
    }

    @GetMapping("/refresh-token")
    @Operation(summary = "Refresh JWT token", security = @SecurityRequirement(name = "bearer-token"), responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = JWTOutputDto.class))),
        @ApiResponse(responseCode = "401", content = @Content),
        @ApiResponse(responseCode = "403", content = @Content)
    })
    public ResponseEntity<JWTOutputDto> refreshToken(HttpServletRequest request) throws ForbiddenException {
        return new ResponseEntity<>(jwtService.refreshToken(request.getHeader(AUTHORIZATION), request.getRequestURL().toString()), OK);
    }
}
