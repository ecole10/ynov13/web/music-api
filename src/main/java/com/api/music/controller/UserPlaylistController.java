package com.api.music.controller;

import com.api.music.dto.input.user.playlist.UserPlaylistInputDto;
import com.api.music.dto.input.user.playlist.UserPlaylistTrackInputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistOutputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistTrackOutputDto;
import com.api.music.service.UserPlaylistService;
import com.api.music.utils.JsonResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "User")
@SecurityRequirement(name = "bearer-token")
@RestController
@RequestMapping("/api/users/me/playlists")
@RequiredArgsConstructor
public class UserPlaylistController {
    private final UserPlaylistService playlistService;

    @GetMapping
    @Operation(summary = "Get user's playlists", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = UserPlaylistOutputDto.class)))),
        @ApiResponse(responseCode = "403", content = @Content)
    })
    public ResponseEntity<List<UserPlaylistOutputDto>> getPlaylists(Principal principal) {
        return new ResponseEntity<>(playlistService.getPlaylists(principal.getName()), OK);
    }

    @PostMapping
    @Operation(summary = "Create a playlist", responses = {
        @ApiResponse(responseCode = "201", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserPlaylistOutputDto.class))),
        @ApiResponse(responseCode = "403", content = @Content),
        @ApiResponse(responseCode = "422", content = @Content)
    })
    public ResponseEntity<UserPlaylistOutputDto> createPlaylist(
        @Valid @RequestBody UserPlaylistInputDto userPlaylistInputDto,
        Principal principal
    ) {
        return new ResponseEntity<>(playlistService.createPlaylist(principal.getName(), userPlaylistInputDto), CREATED);
    }

    @DeleteMapping("/{playlist-id}")
    @Operation(summary = "Remove playlist", responses = {
            @ApiResponse(responseCode = "204", content = @Content),
            @ApiResponse(responseCode = "400", content = @Content),
            @ApiResponse(responseCode = "403", content = @Content),
            @ApiResponse(responseCode = "404", content = @Content)
    })
    public ResponseEntity<Void> removePlaylist(
            @PathVariable(name = "playlist-id") UUID playlistId,
            Principal principal
    ) {
        playlistService.removePlaylist(principal.getName(), playlistId);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @GetMapping("/{playlist-id}/tracks")
    @Operation(summary = "Get user's playlist tracks", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = UserPlaylistTrackOutputDto.class)))),
        @ApiResponse(responseCode = "403", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content),
    })
    public ResponseEntity<List<UserPlaylistTrackOutputDto>> getPlaylistTracks(
        @PathVariable(name = "playlist-id") UUID playlistId,
        Principal principal
    ) {
        return new ResponseEntity<>(playlistService.getPlaylistTracks(principal.getName(), playlistId), OK);
    }

    @PostMapping("/{playlist-id}/tracks")
    @Operation(summary = "Add track to playlist", responses = {
        @ApiResponse(responseCode = "201", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserPlaylistTrackOutputDto.class))),
        @ApiResponse(responseCode = "403", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content),
        @ApiResponse(responseCode = "422", content = @Content)
    })
    public ResponseEntity<UserPlaylistTrackOutputDto> addPlaylistTrack(
        @PathVariable(name = "playlist-id") UUID playlistId,
        @Valid @RequestBody UserPlaylistTrackInputDto userPlaylistTrackInputDto,
        Principal principal
    ) {
        return new ResponseEntity<>(playlistService.addPlaylistTrack(principal.getName(), playlistId, userPlaylistTrackInputDto), CREATED);
    }

    @DeleteMapping("/{playlist-id}/tracks/{track-id}")
    @Operation(summary = "Remove track from playlist", responses = {
        @ApiResponse(responseCode = "204", content = @Content),
        @ApiResponse(responseCode = "400", content = @Content),
        @ApiResponse(responseCode = "403", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content)
    })
    public ResponseEntity<Void> removeTrackFromPlaylist(
        @PathVariable(name = "playlist-id") UUID playlistId,
        @PathVariable(name = "track-id") UUID trackId,
        Principal principal
    ) {
        playlistService.removePlaylistTrack(principal.getName(), playlistId, trackId);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @GetMapping("/{playlist-id}/shared/email")
    @Operation(summary = "Send playlist with email", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = JsonResponse.class))),
        @ApiResponse(responseCode = "403", content = @Content),
        @ApiResponse(responseCode = "404", content = @Content),
    })
    public ResponseEntity<JsonResponse> sendPlaylistWithEmail(
        @PathVariable(name = "playlist-id") UUID playlistId,
        @RequestParam String email,
        Principal principal
    ) {
        playlistService.sendPlaylistWithEmail(principal.getName(), playlistId, email);
        return new ResponseEntity<>(new JsonResponse("Email sent"), OK);
    }
}
