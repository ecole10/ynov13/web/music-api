package com.api.music.dto.input.user.playlist;

import com.api.music.enums.Platform;
import lombok.Data;

@Data
public class UserPlaylistTrackInputDto {
    private String trackId;
    private Platform platform;
}
