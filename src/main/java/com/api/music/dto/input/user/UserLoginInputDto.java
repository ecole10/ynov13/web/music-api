package com.api.music.dto.input.user;

import lombok.Data;

@Data
public class UserLoginInputDto {
    private String username;
    private String password;
}
