package com.api.music.dto.input.user;

import com.api.music.enums.Role;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserRoleInputDto {
    @NotNull
    private Role role;
}
