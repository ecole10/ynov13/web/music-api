package com.api.music.dto.input.user.playlist;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserPlaylistInputDto {
    @NotBlank
    @Size(max = 200)
    private String name;
}
