package com.api.music.dto.output.track;

import lombok.Data;

@Data
public class TrackOutputDto {
    private String id;
    private String title;
    private String[] artists;
    private String album;
    private String albumCover;
    private String link;
    private String preview;
    private String platform;
}
