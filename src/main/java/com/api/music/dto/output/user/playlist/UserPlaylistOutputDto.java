package com.api.music.dto.output.user.playlist;

import lombok.Data;

import java.util.UUID;

@Data
public class UserPlaylistOutputDto {
    private UUID id;
    private String name;
}
