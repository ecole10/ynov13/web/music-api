package com.api.music.dto.output.auth;

import lombok.Data;

@Data
public class JWTOutputDto {
    private String accessToken;
    private String refreshToken;
}
