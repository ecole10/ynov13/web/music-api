package com.api.music.dto.output.user.platform;

import com.api.music.enums.Platform;
import lombok.Data;

@Data
public class UserPlatformTracksOutputDto {
    private Platform platform;
    private Integer numberTracks;
}
