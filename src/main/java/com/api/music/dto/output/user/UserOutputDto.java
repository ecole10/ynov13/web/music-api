package com.api.music.dto.output.user;

import com.api.music.dto.output.role.RoleOutputDto;
import lombok.Data;

import java.util.UUID;

@Data
public class UserOutputDto {
    private UUID id;
    private String username;
    private String email;
    private Boolean enabled;
    private RoleOutputDto role;
}
