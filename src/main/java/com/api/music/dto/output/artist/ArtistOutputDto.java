package com.api.music.dto.output.artist;

import lombok.Data;

@Data
public class ArtistOutputDto {
    private String id;
    private String name;
    private String platform;
}
