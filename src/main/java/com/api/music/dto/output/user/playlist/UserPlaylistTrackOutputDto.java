package com.api.music.dto.output.user.playlist;

import lombok.Data;

import java.util.UUID;

@Data
public class UserPlaylistTrackOutputDto {
    private UUID id;
    private String platformId;
    private String platform;
    private String title;
    private String link;
    private String duration;
}
