package com.api.music.dto.output.role;

import lombok.Data;

import java.util.UUID;

@Data
public class RoleOutputDto {
    private UUID id;
    private String name;
}
