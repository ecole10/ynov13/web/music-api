package com.api.music.dto.output.track;

import lombok.Data;

@Data
public class TrackDetailsOutputDto {
    private String id;
    private String title;
    private String duration;
    private String link;
    private String[] artists;
    private String album;
    private String platform;
}
