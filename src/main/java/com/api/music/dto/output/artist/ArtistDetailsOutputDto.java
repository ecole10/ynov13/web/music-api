package com.api.music.dto.output.artist;

import lombok.Data;

@Data
public class ArtistDetailsOutputDto {
    private String id;
    private String name;
    private Integer followers;
    private String link;
    private String platform;
}
