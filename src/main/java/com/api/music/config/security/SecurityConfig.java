package com.api.music.config.security;

import com.api.music.filter.JWTAuthenticationFilter;
import com.api.music.filter.JWTAuthorizationFilter;
import com.api.music.service.JWTService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static com.api.music.enums.Role.ROLE_ADMIN;
import static com.api.music.enums.Role.ROLE_USER;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Value("${cors.allowed.origins}")
    private String corsAllowedOrigins;

    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleHierarchy roleHierarchy;
    private final JWTService jwtService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.httpBasic().disable();
        http.sessionManagement().sessionCreationPolicy(STATELESS);

        http.cors();
        http.authorizeRequests().antMatchers(GET, "/api/users/me/**").hasAnyAuthority(ROLE_USER.toString());
        http.authorizeRequests().antMatchers(POST, "/api/users/me/**").hasAnyAuthority(ROLE_USER.toString());
        http.authorizeRequests().antMatchers(DELETE, "/api/users/me/**").hasAnyAuthority(ROLE_USER.toString());
        http.authorizeRequests().antMatchers(GET, "/api/users/**").hasAnyAuthority(ROLE_ADMIN.toString());
        http.authorizeRequests().antMatchers(PATCH, "/api/users/**").hasAnyAuthority(ROLE_ADMIN.toString());
        http.authorizeRequests().antMatchers(DELETE, "/api/users/**").hasAnyAuthority(ROLE_ADMIN.toString());

        http.authorizeRequests().antMatchers(GET, "/api/roles").hasAnyAuthority(ROLE_ADMIN.toString());
        http.authorizeRequests().anyRequest().permitAll();

        http.addFilter(new JWTAuthenticationFilter(authenticationManagerBean(), jwtService));
        http.addFilterBefore(new JWTAuthorizationFilter(jwtService), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public DefaultWebSecurityExpressionHandler webSecurityExpressionCustomHandler() {
        DefaultWebSecurityExpressionHandler expressionHandler = new DefaultWebSecurityExpressionHandler();
        expressionHandler.setRoleHierarchy(roleHierarchy);
        return expressionHandler;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**").allowedMethods("*").allowedOrigins(corsAllowedOrigins);
            }
        };
    }
}
