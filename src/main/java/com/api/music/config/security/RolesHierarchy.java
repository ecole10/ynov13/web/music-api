package com.api.music.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;

import static com.api.music.enums.Role.*;

@Configuration
public class RolesHierarchy {
    @Bean
    public RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        String hierarchy = String.format("%s > %s", ROLE_ADMIN, ROLE_USER);
        roleHierarchy.setHierarchy(hierarchy);
        return roleHierarchy;
    }
}
