package com.api.music.config.openapi;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.*;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static com.api.music.enums.Security.LOGIN_URL;
import static io.swagger.v3.oas.models.security.SecurityScheme.Type.HTTP;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI springOpenAPI(@Value("${openapi.project.version}") String apiVersion) {
        return new OpenAPI()
            .components(
                new Components().addSecuritySchemes(
                "bearer-token",
                    new SecurityScheme().type(HTTP).scheme("bearer").bearerFormat("JWT")
                )
            )
            .info(
                new Info().title("Music API")
                .version(apiVersion)
            );
    }

    @Bean
    public OpenApiCustomiser loginEndpointCustomizer(ApplicationContext applicationContext) {
        FilterChainProxy filterChainProxy = applicationContext.getBean("springSecurityFilterChain", FilterChainProxy.class);

        return (openAPI) -> {
            for (SecurityFilterChain filterChain : filterChainProxy.getFilterChains()) {
                Stream<Filter> filters = filterChain.getFilters().stream();
                Objects.requireNonNull(UsernamePasswordAuthenticationFilter.class);
                filters = filters.filter(UsernamePasswordAuthenticationFilter.class::isInstance);
                Optional<UsernamePasswordAuthenticationFilter> optionalFilter = filters.map(UsernamePasswordAuthenticationFilter.class::cast).findAny();

                if (optionalFilter.isPresent()) {
                    UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter = optionalFilter.get();

                    Schema<?> schema = (new ObjectSchema()).addProperties(usernamePasswordAuthenticationFilter.getUsernameParameter(), new StringSchema()).addProperties(usernamePasswordAuthenticationFilter.getPasswordParameter(), new StringSchema());
                    RequestBody requestBody = (new RequestBody()).content((new Content()).addMediaType(String.valueOf(APPLICATION_JSON), (new MediaType()).schema(schema)));
                    ApiResponses apiResponses = new ApiResponses()
                        .addApiResponse(String.valueOf(OK.value()), (new ApiResponse()).description(OK.getReasonPhrase()).content((new Content()).addMediaType(String.valueOf(APPLICATION_JSON), (new MediaType()).schema((new ObjectSchema()).addProperties("accessToken", new StringSchema()).addProperties("refreshToken", new StringSchema())))))
                        .addApiResponse(String.valueOf(UNAUTHORIZED.value()), (new ApiResponse()).description(UNAUTHORIZED.getReasonPhrase()));

                    Operation operation = new Operation()
                            .summary("Login")
                            .addTagsItem("Auth")
                            .requestBody(requestBody)
                            .responses(apiResponses);

                    PathItem pathItem = (new PathItem()).post(operation);
                    openAPI.getPaths().addPathItem(LOGIN_URL.value(), pathItem);
                }
            }
        };
    }
}
