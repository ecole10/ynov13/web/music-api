package com.api.music.enums;

public enum DeezerCodeError {
    QUERY_INVALID(600),
    DATA_NOT_FOUND(800);

    private final Integer value;

    DeezerCodeError(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return value;
    }
}
