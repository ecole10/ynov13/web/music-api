package com.api.music.enums;

public enum Security {
    TOKEN_PREFIX("Bearer "),
    LOGIN_URL("/api/auth/login"),
    REFRESH_TOKEN_URL("/api/auth/refresh-token");

    private final String value;

    Security(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
