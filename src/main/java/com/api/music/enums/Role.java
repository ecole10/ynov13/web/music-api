package com.api.music.enums;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}
