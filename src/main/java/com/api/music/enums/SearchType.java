package com.api.music.enums;

public enum SearchType {
    TRACK("track"),
    ARTIST("artist");

    private final String value;

    SearchType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
