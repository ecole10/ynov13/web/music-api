package com.api.music.enums;

public enum EmailSubject {
    CONFIRM_ACCOUNT("Confirm your account"),
    SHARE_PLAYLIST("Playlist shared with you from Music");

    private final String value;

    EmailSubject(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
