package com.api.music.service;

import com.api.music.dto.input.user.playlist.UserPlaylistTrackInputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistTrackOutputDto;
import com.api.music.model.Playlist;
import com.api.music.model.Track;

import java.util.UUID;

public interface UserTrackService {
    UserPlaylistTrackOutputDto createTrack(Playlist playlist, UserPlaylistTrackInputDto userPlaylistTrackInputDto);
    Track getTrack(UUID trackId);
}
