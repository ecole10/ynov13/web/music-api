package com.api.music.service;

import com.api.music.dto.output.auth.JWTOutputDto;
import com.api.music.exception.ForbiddenException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public interface JWTService {
    JWTOutputDto getAccessToken(User user, String requestUrl);
    JWTOutputDto refreshToken(String authorizationHeader,  String requestUrl) throws ForbiddenException;
    String getUsername(String authorizationHeader);
    Collection<SimpleGrantedAuthority> getAuthorities(String authorizationHeader);
}
