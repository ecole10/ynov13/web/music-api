package com.api.music.service;

import com.api.music.dto.input.user.playlist.UserPlaylistInputDto;
import com.api.music.dto.input.user.playlist.UserPlaylistTrackInputDto;
import com.api.music.dto.output.user.platform.UserPlatformTracksOutputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistOutputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistTrackOutputDto;
import com.api.music.model.User;

import java.util.List;
import java.util.UUID;

public interface UserPlaylistService {
    void createDefaultPlaylist(User user);
    List<UserPlaylistOutputDto> getPlaylists(String username);
    List<UserPlaylistTrackOutputDto> getPlaylistTracks(String username, UUID playlistId);
    UserPlaylistOutputDto createPlaylist(String username, UserPlaylistInputDto userPlaylistInputDto);
    void removePlaylist(String username, UUID playlistId);
    UserPlaylistTrackOutputDto addPlaylistTrack(String username, UUID playlistId, UserPlaylistTrackInputDto userPlaylistTrackInputDto);
    void removePlaylistTrack(String username, UUID playlistId, UUID trackId);
    void sendPlaylistWithEmail(String username, UUID playlistId, String email);
    List<UserPlatformTracksOutputDto> getNumberOfTracksByPlatform(String username);
}
