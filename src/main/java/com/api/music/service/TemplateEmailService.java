package com.api.music.service;

import com.api.music.model.ConfirmationToken;
import com.api.music.model.Playlist;
import com.api.music.model.User;

public interface TemplateEmailService {
    String getTemplateConfirmAccount(User user, ConfirmationToken confirmationToken);
    String getTemplateSharePlaylist(User user, Playlist playlist);
}
