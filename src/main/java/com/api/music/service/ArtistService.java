package com.api.music.service;

import com.api.music.dto.output.artist.ArtistDetailsOutputDto;
import com.api.music.dto.output.artist.ArtistOutputDto;
import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.enums.Platform;

import java.util.List;

public interface ArtistService {
    List<ArtistOutputDto> searchArtists(String search, Platform platform);
    ArtistDetailsOutputDto getArtist(String artistId, Platform platform);
    List<TrackDetailsOutputDto> getArtistTopTracks(String artistId, Platform platform);
}
