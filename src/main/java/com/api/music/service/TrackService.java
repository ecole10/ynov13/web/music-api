package com.api.music.service;

import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.dto.output.track.TrackOutputDto;
import com.api.music.enums.Platform;

import java.util.List;

public interface TrackService {
    List<TrackOutputDto> searchTracks(String search, Platform platform);
    TrackDetailsOutputDto getTrack(String trackId, Platform platform);
    List<TrackOutputDto> getTrendsTracks(Platform platform);
}
