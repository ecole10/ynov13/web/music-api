package com.api.music.service;

public interface ConfirmationTokenService {
    void confirmToken(String token);
}
