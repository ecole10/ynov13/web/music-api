package com.api.music.service;

import com.api.music.enums.EmailSubject;

public interface EmailService {
    void send(String to, String email, EmailSubject subject);
}
