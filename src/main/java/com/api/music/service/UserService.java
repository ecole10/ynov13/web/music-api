package com.api.music.service;

import com.api.music.dto.input.user.UserInputDto;
import com.api.music.dto.output.user.UserOutputDto;
import com.api.music.model.ConfirmationToken;
import com.api.music.model.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
    UserOutputDto createUser(UserInputDto userInputDto);
    User getUserByUsername(String username);
    User getUserById(UUID userId);
    UserOutputDto getCurrentUser(String username);
    List<UserOutputDto> getUsers();
    void enableUser(User user);
    ConfirmationToken createConfirmationToken(User user);
    UserOutputDto updateUserRole(UUID userId, String roleName);
    void deleteUser(UUID userId);
}
