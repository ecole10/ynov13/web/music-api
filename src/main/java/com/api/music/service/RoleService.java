package com.api.music.service;

import com.api.music.dto.output.role.RoleOutputDto;
import com.api.music.model.Role;

import java.util.List;

public interface RoleService {
    Role getDefaultRole();
    List<RoleOutputDto> getRoles();
    Role getRoleByName(String roleName);
}
