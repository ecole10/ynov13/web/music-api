package com.api.music.service.impl;

import com.api.music.dto.input.user.UserInputDto;
import com.api.music.dto.output.user.UserOutputDto;
import com.api.music.exception.ResourceNotFoundException;
import com.api.music.exception.UserAlreadyExistException;
import com.api.music.mapper.UserMapper;
import com.api.music.model.ConfirmationToken;
import com.api.music.model.Role;
import com.api.music.model.User;
import com.api.music.repository.ConfirmationTokenRepository;
import com.api.music.repository.UserRepository;
import com.api.music.service.EmailService;
import com.api.music.service.RoleService;
import com.api.music.service.TemplateEmailService;
import com.api.music.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static com.api.music.enums.EmailSubject.CONFIRM_ACCOUNT;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final RoleService roleService;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final EmailService emailService;
    private final TemplateEmailService templateEmailService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = getUserByUsername(username);
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getName()));

        return new org.springframework.security.core.userdetails.User(
            user.getUsername(),
            user.getPassword(),
            authorities
        );
    }

    @Override
    public UserOutputDto createUser(UserInputDto userInputDto) {
        log.info("Saving new user {}", userInputDto.getUsername());
        checkUserExist(userInputDto.getUsername(), userInputDto.getEmail());

        User user = userMapper.userInputDtoToUser(userInputDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()))
            .setRole(roleService.getDefaultRole());
        user = userRepository.save(user);

        emailService.send(
            user.getEmail(),
            templateEmailService.getTemplateConfirmAccount(user, createConfirmationToken(user)),
            CONFIRM_ACCOUNT
        );

        return userMapper.userToUserOutputDto(user);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username)
            .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), username));
    }

    @Override
    public User getUserById(UUID userId) {
        return userRepository.findById(userId)
            .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), userId));
    }

    @Override
    public UserOutputDto getCurrentUser(String username) {
        User user = getUserByUsername(username);
        return userMapper.userToUserOutputDto(user);
    }

    @Override
    public List<UserOutputDto> getUsers() {
        List<User> users = userRepository.findAll();
        return userMapper.usersToUserOutputDtos(users);
    }

    @Override
    public void enableUser(User user) {
        user.setEnabled(true);
        userRepository.save(user);
    }

    @Override
    public ConfirmationToken createConfirmationToken(User user) {
        ConfirmationToken confirmationToken = ConfirmationToken.builder()
            .token(UUID.randomUUID().toString())
            .createdAt(LocalDateTime.now())
            .expiresAt(LocalDateTime.now().plusMinutes(30))
            .user(user)
            .build();

        confirmationTokenRepository.save(confirmationToken);
        return confirmationToken;
    }

    @Override
    public UserOutputDto updateUserRole(UUID userId, String roleName) {
        User user = getUserById(userId);
        Role role = roleService.getRoleByName(roleName);
        user.setRole(role);
        user = userRepository.save(user);
        return userMapper.userToUserOutputDto(user);
    }

    @Override
    public void deleteUser(UUID userId) {
        User user = getUserById(userId);
        userRepository.delete(user);
    }

    private void checkUserExist(String username, String email) {
        if (userRepository.existsByUsername(username)) {
            throw new UserAlreadyExistException("username", username);
        } else if (userRepository.existsByEmail(email)) {
            throw new UserAlreadyExistException("email", email);
        }
    }
}
