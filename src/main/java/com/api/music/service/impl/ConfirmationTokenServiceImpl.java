package com.api.music.service.impl;

import com.api.music.exception.ResourceNotFoundException;
import com.api.music.exception.TokenAlreadyConfirmedException;
import com.api.music.exception.TokenExpiredException;
import com.api.music.model.ConfirmationToken;
import com.api.music.model.User;
import com.api.music.repository.ConfirmationTokenRepository;
import com.api.music.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.api.music.enums.EmailSubject.CONFIRM_ACCOUNT;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final UserService userService;
    private final EmailService emailService;
    private final TemplateEmailService templateEmailService;
    private final UserPlaylistService playlistService;

    @Override
    public void confirmToken(String token) {
        ConfirmationToken confirmationToken = getToken(token);
        User user = confirmationToken.getUser();

        if (confirmationToken.isAlreadyConfirmed()) {
            throw new TokenAlreadyConfirmedException();
        }

        if (confirmationToken.isExpired()) {
            String confirmAccountTemplate = templateEmailService.getTemplateConfirmAccount(user, userService.createConfirmationToken(user));
            emailService.send(user.getEmail(), confirmAccountTemplate, CONFIRM_ACCOUNT);
            throw new TokenExpiredException();
        }

        playlistService.createDefaultPlaylist(user);
        userService.enableUser(user);
        confirmationToken.setConfirmedAt(LocalDateTime.now());
        confirmationTokenRepository.save(confirmationToken);
    }

    private ConfirmationToken getToken(String token) {
        return confirmationTokenRepository.findByToken(token)
            .orElseThrow(() -> new ResourceNotFoundException(ConfirmationToken.class.getSimpleName(), token));
    }
}
