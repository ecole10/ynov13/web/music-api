package com.api.music.service.impl;

import com.api.music.dto.output.role.RoleOutputDto;
import com.api.music.mapper.RoleMapper;
import com.api.music.model.Role;
import com.api.music.repository.RoleRepository;
import com.api.music.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.api.music.enums.Role.ROLE_USER;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleMapper roleMapper;
    private final RoleRepository roleRepository;

    @Override
    public Role getDefaultRole() {
        return getRoleByName(ROLE_USER.toString());
    }

    @Override
    public List<RoleOutputDto> getRoles() {
        List<Role> roles = roleRepository.findAll();
        return roleMapper.rolesToRoleOutputDtos(roles);
    }

    @Override
    public Role getRoleByName(String roleName) {
        return roleRepository.findByName(roleName);
    }
}
