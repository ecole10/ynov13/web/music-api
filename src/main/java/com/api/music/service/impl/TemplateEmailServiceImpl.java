package com.api.music.service.impl;

import com.api.music.model.ConfirmationToken;
import com.api.music.model.Playlist;
import com.api.music.model.User;
import com.api.music.service.TemplateEmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Service
@RequiredArgsConstructor
public class TemplateEmailServiceImpl implements TemplateEmailService {
    private final SpringTemplateEngine templateEngine;
    @Value("${spring.application.url}")
    private String basePath;

    public String getTemplateConfirmAccount(User user, ConfirmationToken confirmationToken) {
        final Context context = new Context();
        context.setVariable("username", user.getUsername());
        context.setVariable("tokenExpiresAt", confirmationToken.getExpiresAt());
        context.setVariable("token", confirmationToken.getToken());
        context.setVariable("link", String.format("%s/api/auth/register/confirm?token=%s", basePath, confirmationToken.getToken()));
        return templateEngine.process("mail/confirm-account", context);
    }

    @Override
    public String getTemplateSharePlaylist(User user, Playlist playlist) {
        final Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("playlist", playlist);
        context.setVariable("spotifyLogo", String.format("%s/images/spotify.png", basePath));
        context.setVariable("deezerLogo", String.format("%s/images/deezer.png", basePath));
        return templateEngine.process("mail/shared-playlist", context);
    }
}
