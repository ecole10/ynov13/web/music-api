package com.api.music.service.impl;

import com.api.music.enums.EmailSubject;
import com.api.music.exception.EmailNotSendException;
import com.api.music.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender mailSender;
    @Value("${spring.mail.from}")
    private String from;

    @Override
    @Async
    public void send(String to, String email, EmailSubject subject) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();

            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, UTF_8.name());
            messageHelper.setFrom(from);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject.value());
            messageHelper.setText(email, true);

            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            log.error("Failed to send email", e);
            throw new EmailNotSendException();
        }
    }
}
