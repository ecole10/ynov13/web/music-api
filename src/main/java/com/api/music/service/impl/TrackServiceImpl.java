package com.api.music.service.impl;

import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.dto.output.track.TrackOutputDto;
import com.api.music.enums.Platform;
import com.api.music.mapper.deezer.DeezerTrackMapper;
import com.api.music.mapper.spotify.SpotifyTrackMapper;
import com.api.music.service.TrackService;
import com.api.music.webservice.deezer.api.chart.DeezerGetChart;
import com.api.music.webservice.deezer.api.search.DeezerGetSearch;
import com.api.music.webservice.deezer.api.track.DeezerGetTrack;
import com.api.music.webservice.spotify.api.playlist.SpotifyGetPlaylistTracks;
import com.api.music.webservice.spotify.api.search.SpotifyGetSearch;
import com.api.music.webservice.spotify.api.tracks.SpotifyGetTrack;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.api.music.enums.SearchType.TRACK;

@Service
@RequiredArgsConstructor
public class TrackServiceImpl implements TrackService {
    private final SpotifyGetSearch spotifyGetSearch;
    private final SpotifyGetTrack spotifyGetTrack;
    private final SpotifyGetPlaylistTracks spotifyGetPlaylistTracks;
    private final SpotifyTrackMapper spotifyTrackMapper = new SpotifyTrackMapper();
    private final DeezerGetSearch deezerGetSearch;
    private final DeezerGetTrack deezerGetTrack;
    private final DeezerGetChart deezerGetChart;
    private final DeezerTrackMapper deezerTrackMapper = new DeezerTrackMapper();

    @Override
    public List<TrackOutputDto> searchTracks(String search, Platform platform) {
        if (platform == null) {
            return Stream.of(searchSpotifyTracks(search), searchDeezerTracks(search)).flatMap(Collection::stream)
                .collect(Collectors.toList());
        } else {
            switch (platform) {
                case SPOTIFY:
                    return searchSpotifyTracks(search);
                case DEEZER:
                    return searchDeezerTracks(search);
                default:
                    throw new RuntimeException();
            }
        }
    }

    @Override
    public TrackDetailsOutputDto getTrack(String trackId, Platform platform) {
        switch (platform) {
            case SPOTIFY:
                return getSpotifyTrack(trackId);
            case DEEZER:
                return getDeezerTrack(trackId);
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public List<TrackOutputDto> getTrendsTracks(Platform platform) {
        if (platform == null) {
            return Stream.of(getSpotifyTrendsTracks(), getDeezerTrendsTracks()).flatMap(Collection::stream)
                    .collect(Collectors.toList());
        } else {
            switch (platform) {
                case SPOTIFY:
                    return getSpotifyTrendsTracks();
                case DEEZER:
                    return getDeezerTrendsTracks();
                default:
                    throw new RuntimeException();
            }
        }
    }

    private List<TrackOutputDto> searchSpotifyTracks(String search) {
        JsonNode response = spotifyGetSearch.send(search, TRACK.value());
        return spotifyTrackMapper.tracksToTrackOutputDtos(response);
    }

    private List<TrackOutputDto> searchDeezerTracks(String search) {
        JsonNode response = deezerGetSearch.send(search, TRACK.value());
        return deezerTrackMapper.tracksToTrackOutputDtos(response);
    }

    private TrackDetailsOutputDto getSpotifyTrack(String trackId) {
        JsonNode response = spotifyGetTrack.send(trackId);
        return spotifyTrackMapper.trackDetailsToTrackDetailsOutputDto(response);
    }

    private TrackDetailsOutputDto getDeezerTrack(String trackId) {
        JsonNode response = deezerGetTrack.send(trackId);
        return deezerTrackMapper.trackDetailsToTrackDetailsOutputDto(response);
    }

    /**
     * Charts endpoints is not implemented on Spotify API
     * @see <a href="https://github.com/spotify/web-api/issues/33">Github issue</a>
     */
    private List<TrackOutputDto> getSpotifyTrendsTracks() {
        String PLAYLIST_TOP_TRACKS_FR_SPOTIFY = "37i9dQZEVXbIPWwFssbupI";
        JsonNode response = spotifyGetPlaylistTracks.send(PLAYLIST_TOP_TRACKS_FR_SPOTIFY, "10");
        return spotifyTrackMapper.playlistTracksToTrackOutputDtos(response);
    }

    private List<TrackOutputDto> getDeezerTrendsTracks() {
        JsonNode response = deezerGetChart.send("tracks");
        return deezerTrackMapper.tracksToTrackOutputDtos(response);
    }
}
