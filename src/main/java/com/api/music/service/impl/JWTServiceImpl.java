package com.api.music.service.impl;

import com.api.music.dto.output.auth.JWTOutputDto;
import com.api.music.exception.AuthorizationExceptedException;
import com.api.music.exception.ForbiddenException;
import com.api.music.service.JWTService;
import com.api.music.service.UserService;
import com.api.music.utils.JWTUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static com.api.music.enums.Security.TOKEN_PREFIX;
import static java.util.Arrays.stream;

@Service
@RequiredArgsConstructor
public class JWTServiceImpl implements JWTService {
    private final JWTUtils jwtUtils;
    private final UserService userService;

    @Override
    public JWTOutputDto getAccessToken(User user, String requestUrl) {
        String accessToken = jwtUtils.createAccessToken(user, requestUrl);
        String refreshToken = jwtUtils.createRefreshToken(user.getUsername(), requestUrl);
        return jwtUtils.formatResponseTokens(accessToken, refreshToken);
    }

    @Override
    public JWTOutputDto refreshToken(String authorizationHeader, String requestUrl) throws ForbiddenException {
        if (authorizationHeader != null && authorizationHeader.startsWith(TOKEN_PREFIX.value())) {
            try {
                return getNewAccessToken(authorizationHeader, requestUrl);
            } catch (Exception e) {
                throw new ForbiddenException("Token invalid");
            }
        } else {
            throw new AuthorizationExceptedException();
        }
    }

    @Override
    public String getUsername(String authorizationHeader) {
        String token = removeTokenPrefix(authorizationHeader);
        return jwtUtils.extractUsernameFromToken(token);
    }

    @Override
    public Collection<SimpleGrantedAuthority> getAuthorities(String authorizationHeader) {
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        String token = removeTokenPrefix(authorizationHeader);
        String[] roles = jwtUtils.verifyToken(token).getClaim("roles").asArray(String.class);
        stream(roles).forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
        return authorities;
    }

    private JWTOutputDto getNewAccessToken(String authorizationHeader, String requestUrl) {
        String refreshToken = authorizationHeader.substring(TOKEN_PREFIX.value().length());
        String username = jwtUtils.extractUsernameFromToken(refreshToken);
        String accessToken = jwtUtils.createAccessToken(userService.getUserByUsername(username), requestUrl);
        return jwtUtils.formatResponseTokens(accessToken, refreshToken);
    }

    private String removeTokenPrefix(String token) {
        return token.substring(TOKEN_PREFIX.value().length());
    }
}
