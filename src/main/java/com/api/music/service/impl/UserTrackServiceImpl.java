package com.api.music.service.impl;

import com.api.music.dto.input.user.playlist.UserPlaylistTrackInputDto;
import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistTrackOutputDto;
import com.api.music.exception.ResourceAlreadyExistException;
import com.api.music.exception.ResourceNotFoundException;
import com.api.music.mapper.UserTrackMapper;
import com.api.music.model.Playlist;
import com.api.music.model.Track;
import com.api.music.repository.TrackRepository;
import com.api.music.service.TrackService;
import com.api.music.service.UserTrackService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserTrackServiceImpl implements UserTrackService {
    private final TrackRepository trackRepository;
    private final UserTrackMapper userTrackMapper;
    private final TrackService trackService;

    @Override
    public UserPlaylistTrackOutputDto createTrack(Playlist playlist, UserPlaylistTrackInputDto userPlaylistTrackInputDto) {
        Track track;

        if (trackRepository.existsByPlatformAndPlatformId(userPlaylistTrackInputDto.getPlatform().name(), userPlaylistTrackInputDto.getTrackId())) {
            track = trackRepository.findByPlatformAndPlatformId(
                userPlaylistTrackInputDto.getPlatform().name(),
                userPlaylistTrackInputDto.getTrackId()
            );

            if (!track.isContainsPlaylist(playlist)) {
                track.getPlaylists().add(playlist);
            } else {
                throw new ResourceAlreadyExistException(
                    Track.class.getSimpleName(),
                    track.getTitle(),
                    Playlist.class.getSimpleName(),
                    playlist.getName()
                );
            }
        } else {
            TrackDetailsOutputDto trackDetailsOutputDto = trackService.getTrack(
                userPlaylistTrackInputDto.getTrackId(),
                userPlaylistTrackInputDto.getPlatform()
            );

            track = new Track()
                .setPlatformId(trackDetailsOutputDto.getId())
                .setPlatform(trackDetailsOutputDto.getPlatform())
                .setTitle(trackDetailsOutputDto.getTitle())
                .setDuration(trackDetailsOutputDto.getDuration())
                .setLink(trackDetailsOutputDto.getLink())
                .setPlaylists(Collections.singletonList(playlist));
        }

        trackRepository.save(track);
        return userTrackMapper.trackToUserPlaylistTrackOutputDto(track);
    }

    @Override
    public Track getTrack(UUID trackId) {
        return trackRepository.findById(trackId)
            .orElseThrow(() -> new ResourceNotFoundException(Track.class.getSimpleName(), trackId));
    }
}
