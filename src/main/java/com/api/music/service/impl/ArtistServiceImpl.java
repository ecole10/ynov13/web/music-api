package com.api.music.service.impl;

import com.api.music.dto.output.artist.ArtistDetailsOutputDto;
import com.api.music.dto.output.artist.ArtistOutputDto;
import com.api.music.dto.output.track.TrackDetailsOutputDto;
import com.api.music.enums.Platform;
import com.api.music.mapper.deezer.DeezerArtistMapper;
import com.api.music.mapper.spotify.SpotifyArtistMapper;
import com.api.music.service.ArtistService;
import com.api.music.webservice.deezer.api.artist.DeezerGetArtist;
import com.api.music.webservice.deezer.api.artist.DeezerGetArtistTopTracks;
import com.api.music.webservice.deezer.api.search.DeezerGetSearch;
import com.api.music.webservice.spotify.api.artists.SpotifyGetArtist;
import com.api.music.webservice.spotify.api.artists.SpotifyGetArtistTopTracks;
import com.api.music.webservice.spotify.api.search.SpotifyGetSearch;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.api.music.enums.SearchType.ARTIST;

@Service
@RequiredArgsConstructor
public class ArtistServiceImpl implements ArtistService {
    private final SpotifyGetSearch spotifyGetSearch;
    private final SpotifyGetArtist spotifyGetArtist;
    private final SpotifyGetArtistTopTracks spotifyGetArtistTopTracks;
    private final SpotifyArtistMapper spotifyArtistMapper = new SpotifyArtistMapper();
    private final DeezerGetSearch deezerGetSearch;
    private final DeezerGetArtist deezerGetArtist;
    private final DeezerGetArtistTopTracks deezerGetArtistTopTracks;
    private final DeezerArtistMapper deezerArtistMapper = new DeezerArtistMapper();

    @Override
    public List<ArtistOutputDto> searchArtists(String search, Platform platform) {
        if (platform == null) {
            return Stream.of(searchSpotifyArtists(search), searchDeezerArtists(search)).flatMap(Collection::stream)
                .collect(Collectors.toList());
        } else {
            switch (platform) {
                case SPOTIFY:
                    return searchSpotifyArtists(search);
                case DEEZER:
                    return searchDeezerArtists(search);
                default:
                    throw new RuntimeException();
            }
        }
    }

    @Override
    public ArtistDetailsOutputDto getArtist(String artistId, Platform platform) {
        switch (platform) {
            case SPOTIFY:
                return getSpotifyArtist(artistId);
            case DEEZER:
                return getDeezerArtist(artistId);
            default:
                throw new RuntimeException();
        }
    }

    @Override
    public List<TrackDetailsOutputDto> getArtistTopTracks(String artistId, Platform platform) {
        switch (platform) {
            case SPOTIFY:
                return getSpotifyArtistTopTracks(artistId);
            case DEEZER:
                return getDeezerArtistTopTracks(artistId);
            default:
                throw new RuntimeException();
        }
    }

    private List<ArtistOutputDto> searchSpotifyArtists(String search) {
        JsonNode response = spotifyGetSearch.send(search, ARTIST.value());
        return spotifyArtistMapper.artistsToArtistOutputDtos(response);
    }

    private List<ArtistOutputDto> searchDeezerArtists(String search) {
        JsonNode response = deezerGetSearch.send(search, ARTIST.value());
        return deezerArtistMapper.artistsToArtistOutputDtos(response);
    }

    private ArtistDetailsOutputDto getSpotifyArtist(String artistId) {
        JsonNode response = spotifyGetArtist.send(artistId);
        return spotifyArtistMapper.artistDetailsToArtistDetailsOutputDto(response);
    }

    private ArtistDetailsOutputDto getDeezerArtist(String artistId) {
        JsonNode response = deezerGetArtist.send(artistId);
        return deezerArtistMapper.artistDetailsToArtistDetailsOutputDto(response);
    }

    private List<TrackDetailsOutputDto> getSpotifyArtistTopTracks(String artistId) {
        JsonNode response = spotifyGetArtistTopTracks.send(artistId);
        return spotifyArtistMapper.artistTopTracksToTrackDetailsOutputDtos(response);
    }

    private List<TrackDetailsOutputDto> getDeezerArtistTopTracks(String artistId) {
        JsonNode response = deezerGetArtistTopTracks.send(artistId);
        return deezerArtistMapper.artistTopTracksToTrackDetailsOutputDtos(response);
    }
}
