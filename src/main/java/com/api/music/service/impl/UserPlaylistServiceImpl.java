package com.api.music.service.impl;

import com.api.music.dto.input.user.playlist.UserPlaylistInputDto;
import com.api.music.dto.input.user.playlist.UserPlaylistTrackInputDto;
import com.api.music.dto.output.user.platform.UserPlatformTracksOutputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistOutputDto;
import com.api.music.dto.output.user.playlist.UserPlaylistTrackOutputDto;
import com.api.music.enums.Platform;
import com.api.music.exception.ElementNotFoundInResourceException;
import com.api.music.exception.ResourceNotFoundException;
import com.api.music.mapper.UserPlaylistMapper;
import com.api.music.mapper.UserTrackMapper;
import com.api.music.model.Playlist;
import com.api.music.model.Track;
import com.api.music.model.User;
import com.api.music.repository.PlaylistRepository;
import com.api.music.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.api.music.enums.EmailSubject.SHARE_PLAYLIST;

@Service
@RequiredArgsConstructor
public class UserPlaylistServiceImpl implements UserPlaylistService {
    private final PlaylistRepository playlistRepository;
    private final UserPlaylistMapper userPlaylistMapper;
    private final UserService userService;
    private final UserTrackService userTrackService;
    private final UserTrackMapper userTrackMapper;
    private final EmailService emailService;
    private final TemplateEmailService templateEmailService;

    @Override
    public void createDefaultPlaylist(User user) {
        Playlist playlist = new Playlist()
            .setName("Music")
            .setUser(user);
        playlistRepository.save(playlist);
    }

    @Override
    public List<UserPlaylistOutputDto> getPlaylists(String username) {
        User user = userService.getUserByUsername(username);
        List<Playlist> playlists = playlistRepository.findAllByUserId(user.getId());
        return userPlaylistMapper.playlistsToUserPlaylistOutputDtos(playlists);
    }

    @Override
    public List<UserPlaylistTrackOutputDto> getPlaylistTracks(String username, UUID playlistId) {
        Playlist playlist = getPlaylistOwnedByUser(playlistId, username);
        return userTrackMapper.tracksToUserPlaylistTrackOutputDtos(new ArrayList<>(playlist.getTracks()));
    }

    @Override
    public UserPlaylistOutputDto createPlaylist(String username, UserPlaylistInputDto userPlaylistInputDto) {
        Playlist playlist = userPlaylistMapper.userPlaylistInputDtoToPlaylist(userPlaylistInputDto);
        playlist.setUser(userService.getUserByUsername(username));
        playlistRepository.save(playlist);
        return userPlaylistMapper.playlistToUserPlaylistOutputDto(playlist);
    }

    @Override
    public void removePlaylist(String username, UUID playlistId) {
        Playlist playlist = getPlaylistOwnedByUser(playlistId, username);
        playlistRepository.delete(playlist);
    }

    @Override
    public UserPlaylistTrackOutputDto addPlaylistTrack(
        String username,
        UUID playlistId,
        UserPlaylistTrackInputDto userPlaylistTrackInputDto
    ) {
        Playlist playlist = getPlaylistOwnedByUser(playlistId, username);
        return userTrackService.createTrack(playlist, userPlaylistTrackInputDto);
    }

    @Override
    public void removePlaylistTrack(String username, UUID playlistId, UUID trackId) {
        Playlist playlist = getPlaylistOwnedByUser(playlistId, username);
        Track track = userTrackService.getTrack(trackId);

        if (playlist.isContainsTrack(track)) {
            playlist.getTracks().remove(track);
            playlistRepository.save(playlist);
        } else {
            throw new ElementNotFoundInResourceException(
                Track.class.getSimpleName(),
                track.getTitle(),
                Playlist.class.getSimpleName(),
                playlist.getName()
            );
        }
    }

    @Override
    public void sendPlaylistWithEmail(String username, UUID playlistId, String email) {
        User user = userService.getUserByUsername(username);
        Playlist playlist = getPlaylistByIdAndUser(playlistId, user);
        String sharePlaylistTemplate = templateEmailService.getTemplateSharePlaylist(user, playlist);
        emailService.send(email, sharePlaylistTemplate, SHARE_PLAYLIST);
    }

    @Override
    public List<UserPlatformTracksOutputDto> getNumberOfTracksByPlatform(String username) {
        User user = userService.getUserByUsername(username);
        Map<String, Integer> numberTracksByPlatform = new HashMap<>();

        for (Playlist playlist : user.getPlaylists()) {
            for (Track track : playlist.getTracks()) {
                numberTracksByPlatform.put(
                    track.getPlatform(),
                    numberTracksByPlatform.getOrDefault(track.getPlatform(), 0) + 1
                );
            }
        }

        List<UserPlatformTracksOutputDto> platforms = new ArrayList<>();
        numberTracksByPlatform.forEach((platform, numberTracks) -> {
            platforms.add(
                new UserPlatformTracksOutputDto()
                    .setPlatform(Platform.valueOf(platform))
                    .setNumberTracks(numberTracks)
            );
        });

        return platforms;
    }

    private Playlist getPlaylistOwnedByUser(UUID playlistId, String username) {
        User user = userService.getUserByUsername(username);
        return getPlaylistByIdAndUser(playlistId, user);
    }

    private Playlist getPlaylistByIdAndUser(UUID playlistId, User user) {
        return playlistRepository.findByIdAndUser(playlistId, user)
            .orElseThrow(() -> new ResourceNotFoundException(Playlist.class.getSimpleName(), playlistId));
    }
}
