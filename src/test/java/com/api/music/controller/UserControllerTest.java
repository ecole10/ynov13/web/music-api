package com.api.music.controller;

import com.api.music.SecurityConfig;
import com.api.music.dto.output.user.UserOutputDto;
import com.api.music.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@Import(SecurityConfig.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private UserService userService;

    @Test
    @WithAnonymousUser
    public void whenAnonymous_shouldGetUsersReturns403() throws Exception {
        mvc.perform(get("/api/users"))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="user", roles={"USER"})
    public void whenLoggedInAsUser_shouldGetUsersReturns403() throws Exception {
        mvc.perform(get("/api/users"))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="admin", roles={"ADMIN"})
    public void whenLoggedInAsAdmin_shouldGetUsers() throws Exception {
        List<UserOutputDto> users = List.of(
                new UserOutputDto()
                    .setId(UUID.randomUUID())
                    .setUsername("user")
                    .setEmail("john@doe.com"),
                new UserOutputDto()
                    .setId(UUID.randomUUID())
                    .setUsername("admin")
                    .setEmail("john@smith.com")
        );

        given(userService.getUsers())
                .willReturn(users);

        mvc.perform(get("/api/users"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.length()", is(2)))
            .andExpect(jsonPath("$.[0].username", is("user")))
            .andExpect(jsonPath("$.[1].username", is("admin")));
    }
}
