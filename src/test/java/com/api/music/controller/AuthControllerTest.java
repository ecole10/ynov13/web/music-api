package com.api.music.controller;

import com.api.music.SecurityConfig;
import com.api.music.dto.input.user.UserInputDto;
import com.api.music.dto.output.role.RoleOutputDto;
import com.api.music.dto.output.user.UserOutputDto;
import com.api.music.service.ConfirmationTokenService;
import com.api.music.service.EmailService;
import com.api.music.service.TemplateEmailService;
import com.api.music.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static com.api.music.enums.Role.ROLE_USER;
import static com.api.music.enums.Security.TOKEN_PREFIX;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthController.class)
@Import(SecurityConfig.class)
public class AuthControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private UserService userService;
    @MockBean
    private JavaMailSender javaMailSender;
    @MockBean
    private EmailService emailService;
    @MockBean
    private TemplateEmailService templateEmailService;
    @MockBean
    private ConfirmationTokenService confirmationTokenService;

    @Test
    @WithAnonymousUser
    public void shouldRegister() throws Exception {
        UserInputDto userInputDto = new UserInputDto()
            .setUsername("user")
            .setEmail("jhon@doe.com")
            .setPassword("password");

        UserOutputDto userOutputDto = new UserOutputDto()
            .setId(UUID.fromString("f730135e-8330-4017-82a5-efe06f88763b"))
            .setUsername("user")
            .setEmail("jhon@doe.com")
            .setRole(new RoleOutputDto().setId(UUID.randomUUID()).setName(ROLE_USER.toString()));

        given(userService.createUser(userInputDto))
            .willReturn(userOutputDto);

        mvc.perform(
            post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(userInputDto)))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id", is("f730135e-8330-4017-82a5-efe06f88763b")))
            .andExpect(jsonPath("$.username", is("user")));
    }

    @Test
    public void whenSendRefreshTokenInAuthorizationHeader_shouldReturnRefreshToken() throws Exception {
        mvc.perform(
            get("/api/auth/refresh-token")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, TOKEN_PREFIX.value()))
            .andExpect(status().isOk());
    }
}
