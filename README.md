# Music API


[![pipeline status](https://gitlab.com/side-projects12/music/music-api/badges/develop/pipeline.svg)](https://gitlab.com/side-projects12/music/music-api/-/commits/develop)
[![coverage report](https://gitlab.com/side-projects12/music/music-api/badges/develop/coverage.svg)](https://gitlab.com/side-projects12/music/music-api/-/commits/develop)

## Stack

- [SPRING BOOT](https://spring.io/projects/spring-boot) 2.5.5
- [POSTGRES](https://www.postgresql.org/) 14

## Requirements

- [Java 11](https://openjdk.java.net/install/)
- [Docker](https://www.docker.com/get-started)

## Install

1. Start docker services

```bash
docker-compose up -d
```

2. Install dependencies

```bash
mvn clean install
```

3. Create dev properties

```bash
touch src/main/resources/application-dev.properties
```

Override default properties :

```text
spring.security.oauth2.client.registration.spotify.client-id=SPOTIFY_CLIENT_ID
spring.security.oauth2.client.registration.spotify.client-secret=SPOTIFY_CLIENT_SECRET

spring.security.oauth2.client.registration.deezer.client-id=DEEZER_CLIENT_ID
spring.security.oauth2.client.registration.deezer.client-secret=DEEZER_CLIENT_SECRET
```

4. Start Spring Boot

```bash
mvn spring-boot:run
```

## API Docs

- [Swagger](http://localhost:8080/swagger-ui.html)
- [OpenApi](http://localhost:8080/api-docs)

## Mailhog

[Webmail](http://localhost:8025)

## Demo User

```text
username : admin
password : adminadmin
```